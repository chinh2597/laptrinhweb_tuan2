var scene = new THREE.Scene();
scene.background = new THREE.Color(0x602b08);
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
var renderer = new THREE.WebGLRenderer();

renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

Tower = function(radTop, radBot, height) {
  var geometry = new THREE.CylinderGeometry(radTop, radBot, height);
  var material = new THREE.MeshBasicMaterial({
    color: 0xf44242,
    wireframe: true
  });
  var cylinder = new THREE.Mesh(geometry, material);
  return cylinder;
}

Bridge = function(width, height, depth) {
  var geometry = new THREE.BoxGeometry(width, height, depth);
  var material = new THREE.MeshBasicMaterial({
    color: 0xC0C0C0,
    wireframe: true
  });
  var cube = new THREE.Mesh(geometry, material);
  return cube;
}

Roof = function(radius, height) {
  var geometry = new THREE.ConeGeometry(radius, height);
  var material = new THREE.MeshBasicMaterial({
    color: 0xffff00,
    wireframe: true
  });
  var cone = new THREE.Mesh(geometry, material);
  return cone;
}

Tower1 = new Tower(1, 1, 4.5);
Tower2 = new Tower(0.5, 0.5, 3.5);
Brige = new Bridge(2.5, 1, 1);
Roof1 = new Roof(1, 2);
Roof2 = new Roof(0.5, 1);

scene.add(Tower1);
scene.add(Tower2);
scene.add(Brige);
scene.add(Roof1);
scene.add(Roof2);

Tower1.position.set(-2, -0.5, 0);
Tower2.position.set(2, -1, 0);
Brige.position.set(0.25, -1, 0);
Roof1.position.set(-2, 2.75, 0);
Roof2.position.set(2, 1.25, 0);

camera.position.z = 5;

function render() {
  renderer.render(scene, camera);
}

render();
controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.addEventListener('change', render);
function zoom(isZoomOut, scale) {
  if (isZoomOut) {
    controls.dollyIn(scale);
  } else {
    controls.dollyOut(scale);
  }
}
$(document).ready(function() {
  $("#zoomInBtn").click(function() {
    zoom(true, 4);
  });

  $("#zoomOutBtn").click(function() {
    zoom(false, 4);
  });
});




